<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Frontend extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->config('email');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('email');
		$this->load->model('Frontend_model', 'frontend');
		$this->load->model('Blog_model', 'blog');
		login_member();
	}

	public function index()
	{
		is_active_maintenance();
		$data = $this->siteSettings();
		$data['page'] = "home_menu";
		$data['blog'] = $this->blog->getActicleHome();
		$data['testimoni'] = $this->frontend->get_testimoni();
		$data['about'] = $this->frontend->view_data("6");
		$data['about_bawah'] = $this->frontend->view_data("7");
		$data['paket'] = $this->frontend->view_data("8");
		$data['paket_pilih'] = $this->frontend->view_paket();
		$this->load->view('theme/header', $data);
		$this->load->view('front/home');
		$this->load->view('theme/footer', $data);
		$this->load->view('theme/script_home_notlogin');
	}

	public function about()
	{
		is_active_maintenance();
		$data = $this->siteSettings();
		$data['about_atas'] = $this->frontend->view_data("6");
		$data['about_bawah'] = $this->frontend->view_data("7");
		$data['page'] = "single_page_menu";
		$this->load->view('theme/header', $data);
		$this->load->view('front/about', $data);
		$this->load->view('theme/footer', $data);
	}

	public function service()
	{
		is_active_maintenance();
		$data = $this->siteSettings();
		$data['paket'] = $this->frontend->view_data("8");
		$data['paket_pilih'] = $this->frontend->view_paket();
		$data['page'] = "single_page_menu";
		$this->load->view('theme/header', $data);
		$this->load->view('front/service', $data);
		$this->load->view('theme/footer', $data);
	}

	public function contact()
	{
		is_active_maintenance();
		$data = $this->siteSettings();
		$data['page'] = "single_page_menu";
		$this->load->view('theme/header', $data);
		$this->load->view('front/contact');
		$this->load->view('theme/footer', $data);
	}

	public function free_cv_template()
	{
		is_active_maintenance();
		$data = $this->siteSettings();
		$data['page'] = "single_page_menu";
		$this->load->view('theme/header', $data);
		$this->load->view('front/free_cv_template');
		$this->load->view('theme/footer', $data);
	}

	public function download_cv_template()
	{
		is_active_maintenance();
		force_download('assets/uploads/template/free_cv_template.pdf', NULL);
	}

	public function siteSettings()
	{
		$data['is_login'] = $this->session->userdata('id_student');
		$data['footer_kiri'] = $this->frontend->view_data("1");
		$data['footer_tengah'] = $this->frontend->view_data("2");
		$data['footer_kanan'] = $this->frontend->view_data("3");
		$data['footer_bawah'] = $this->frontend->view_data("4");
		$data['logo'] = $this->frontend->view_data("5");
		$data['menu_header'] = $this->frontend->view_menu("not_login");
		$data['submenu_header'] = $this->frontend->view_submenu();
		return $data;
	}

	public function register_member()
	{
		$name = $this->input->post('full_name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
		$id_paket = $this->input->post('id_paket');
		$is_active = 1;
		$data = [
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'password' => $password,
			'paket' => $id_paket,
			'is_active' => $is_active
		];
		if ($name != null && $email != null && $phone != null && $password != null && $id_paket != null) {
			$check_user = $this->frontend->check_users($email, $phone);
			if ($check_user > 0) {
				$this->session->set_flashdata('message', '<div class="alert alert-success" id="alert-success" role="alert" style="width: 350px; position:absolute; right:20px;">User sudah tersedia, silahkan login.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>');
				redirect('service');
			} else {
				$this->frontend->register_member($data);
				$this->send_cara_bayar($id_paket, $email, $name);
				$this->session->set_flashdata('message', '<div class="alert alert-success" id="alert-success" role="alert" style="width: 350px; position:absolute; right:20px;">Registrasi berhasil, silahkan login.
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>');
				redirect('service');
			}
		}
	}

	public function send_cara_bayar($id_paket, $email, $name)
	{
		$config_email = $this->frontend->config_email_bayar();
		$get_paket = $this->frontend->get_data_paket($id_paket);
		$before_replace = array('$nama', '$paket_name', '$paket_price');
		$after_replace = array($name, $get_paket->nama_paket, "Rp. " . number_format($get_paket->harga_paket, 2, ',', '.'));
		$from = $this->config->item('smtp_user');
		$to = $email;
		$subject = $config_email->judul;
		$message = str_replace($before_replace, $after_replace, $config_email->content);

		$this->email->set_newline("\r\n");
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);

		if ($this->email->send()) {
			echo 'Your Email has successfully been sent.';
		} else {
			show_error($this->email->print_debugger());
		}
	}

	public function auth()
	{
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		// $data['web'] = $this->management->get_data();
		if ($this->form_validation->run() == false) {
			if ($this->is_logged_in()) {
				redirect('home');
			}
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');

		$user = $this->db->get_where('cms_user_student', array('phone' => $phone))->row_array();
		if ($user != null) {
			if ($user['is_active'] == 1) {
				if (password_verify($password, $user['password'])) {
					$data = [
						'id_student' => $user['id'],
						'full_name_student' => $user['name'],
						'email_student' => $user['email'],
						'phone_student' => $user['phone'],
						'status_paket_student' => $user['status_paket'],
						'paket_student' => $user['paket']
					];
					$this->session->set_userdata($data);
					redirect('member/home');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Password salah!
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>');
					redirect('home');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Belum aktif!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
				redirect('home');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Belum terdaftar!
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>');
			redirect('home');
		}
	}

	public function is_logged_in()
	{
		$user = $this->session->userdata('id_student');
		return isset($user);
	}

	public function maintenance()
	{
		$ci = get_instance();
		if (!$ci->session->userdata('email')) {
			$this->load->view('front/maintenance');
		} else {
			redirect('home');
		}
	}

	public function forgot_password()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ($this->form_validation->run() == false) {
			// $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Belum terdaftar!</div>');
			redirect('home');
		} else {
			$email = $this->input->post('email');
			$user = $this->db->get_where('cms_user_student', ['email' => $email, 'is_active' => 1])->row_array();
			if ($user) {
				$token = base64_encode(random_bytes(32));
				$user_token = [
					'email' => $email,
					'token' => $token
				];
				$this->db->insert('cms_user_student_token', $user_token);
				$this->send_email_forgot_password($email, $token);

				$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert" style="width: 30%; position:absolute; right:20px;">Cek email anda untuk reset password akun anda.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
				redirect('home');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Email belum terdaftar atau aktif!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
				redirect('home');
			}
		}
	}

	public function send_email_forgot_password($email, $token)
	{
		$user = $this->db->get_where('cms_user_student', array('email' => $email))->row();
		$from = $this->config->item('smtp_user');
		$to = $email;
		$subject = "Graduatebuddy : Reset password";
		$message = "Halo, " . $user->name . "<br/>
		<br/>
		Seseorang, semoga anda, telah meminta mengatur ulang password anda untuk akun Graduatebuddy anda di https://graduatebuddy.id.
		<br/>
		<br/> 
		Jika anda tidak melakukan permintaan ini, anda dapat dengan aman mengabaikan email ini.
		<br/>
		<br/>
		Jika tidak, klik link dibawah ini untuk menyelesaikan proses.
		<br/>
		<br/>
		<a href='" . base_url() . 'reset_password?email=' . $email . '&token=' . urlencode($token) . "'>Reset Password</a>";

		$this->email->set_newline("\r\n");
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);

		if ($this->email->send()) {
			echo 'Your Email has successfully been sent.';
		} else {
			show_error($this->email->print_debugger());
		}
	}

	public function reset_password()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('cms_user_student', ['email' => $email])->row_array();
		if ($user) {
			$user_token = $this->db->get_where('cms_user_student_token', ['token' => $token])->row_array();
			if ($user_token) {
				$this->session->set_userdata('reset_email', $email);
				$this->db->set('status', 1);
				$this->db->where('email', $email);
				$this->db->update('cms_user_student_token');
				$this->rubah_password();
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Reset password gagal, token tidak valid!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
				redirect('home');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" style="width: 350px; position:absolute; right:20px;">Reset password gagal, email salah!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
			redirect('home');
		}
	}

	public function rubah_password()
	{
		if (!$this->session->userdata('reset_email')) {
			redirect('home');
		}

		$this->form_validation->set_rules('password1', 'Password', 'trim|required');
		$this->form_validation->set_rules('password2', 'Password', 'trim|required');
		if ($this->form_validation->run() == false) {
			is_active_maintenance();
			$data = $this->siteSettings();
			$data['page'] = "single_page_menu";
			$this->load->view('theme/header', $data);
			$this->load->view('front/rubah_password', $data);
			$this->load->view('theme/footer', $data);
		} else {
			$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
			$email = $this->session->userdata('reset_email');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('cms_user_student');

			$this->session->unset_userdata('reset_email');
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert" style="width: 350px; position:absolute; right:20px;">Reset password berhasil, silahkan login!
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>');
			redirect('home');
		}
	}
}
