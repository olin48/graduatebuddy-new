<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('management_model', 'management');
    }

    public function index()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $data['web'] = $this->management->get_data();
        if ($this->form_validation->run() == false) {
            if ($this->is_logged_in()) {
                $role_id = $this->session->userdata('role_id');
                if ($role_id == 1) {
                    redirect('developer/dashboard');
                } else if ($role_id == 2) {
                    redirect('cms/dashboard');
                } else if ($role_id == 3) {
                    redirect('cms/dashboard');
                }
            } else {
                $data['menu_title'] = "Login Page";
                $this->load->view('auth/login', $data);
            }
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $validEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        $user = null;
        $field = null;
        if ($validEmail == FALSE) {
            $user = $this->db->get_where('cms_user', array('username' => $email))->row_array();
            $field = "Username";
        } else {
            $user = $this->db->get_where('cms_user', array('email' => $email))->row_array();
            $field = "Email";
        }

        if ($user != null) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'id' => $user['id'],
                        'username' => $user['username'],
                        'email' => $user['email'],
                        'role_id' => $user['role_id'],
                        'id_kabkota' => $user['id_kabkota'],
                        'id_korwil' => $user['id_korwil']
                    ];
                    $this->session->set_userdata($data);
                    if ($user['role_id'] == 1) {
                        redirect('developer/dashboard');
                    } else if ($user['role_id'] == 2) {
                        redirect('admin/dashboard');
                    } else if ($user['role_id'] == 3) {
                        redirect('admin/dashboard');
                    } else if ($user['role_id'] == 4) {
                        redirect('admin/dashboard');
                    } else if ($user['role_id'] == 5) {
                        redirect('admin/dashboard');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah!</div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $field . ' belum aktif!</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $field . ' belum terdaftar!</div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('id_kabkota');
        $this->session->unset_userdata('id_korwil');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda telah logout!</div>');
        redirect('auth');
    }

    public function is_logged_in()
    {
        $user = $this->session->userdata('id');
        return isset($user);
    }
}
