<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cms extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        $this->load->model('Role_model', 'role');
        $this->load->model('management_model', 'management');
        $this->load->model('view_users_model', 'view_user');
        $this->load->model('users_admin_model', 'users_admin');
        $this->load->model('member_model', 'member');
        $this->load->model('header_model', 'header_set');
        $this->load->model('footer_model', 'footer_set');
        $this->load->model('menu_model', 'menu');
        $this->load->model('submenu_model', 'submenu');
        $this->load->model('post_blog_model', 'blog');
        $this->load->model('post_lowongan_model', 'lowongan');
        $this->load->model('paket_model', 'paket');
        $this->load->model('pdf_model', 'pdf');
        $this->load->model('testimoni_model', 'testimoni');
        $this->load->model('Frontend_model', 'frontend');
        // is_logged_in();
    }

    public function dashboard()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu_title'] = "";
        $data['url'] = "cms/dashboard";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('dashboard');
        $this->load->view('templates/footer', $config);
    }

    public function auth()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $data['web'] = $this->management->get_data();
        if ($this->form_validation->run() == false) {
            if ($this->is_logged_in()) {
                $role_id = $this->session->userdata('role_id');
                if ($role_id == 1) {
                    redirect('developer/dashboard');
                } else if ($role_id == 2) {
                    redirect('cms/dashboard');
                } else if ($role_id == 3) {
                    redirect('cms/dashboard');
                }
            } else {
                $data['menu_title'] = "Login Page";
                $this->load->view('cms/login', $data);
            }
        } else {
            $this->_login();
        }
    }

    private function _login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $validEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        $user = null;
        $field = null;
        if ($validEmail == FALSE) {
            $user = $this->db->get_where('cms_user', array('username' => $email))->row_array();
            $field = "Username";
        } else {
            $user = $this->db->get_where('cms_user', array('email' => $email))->row_array();
            $field = "Email";
        }

        if ($user != null) {
            if ($user['is_active'] == 1) {
                if (password_verify($password, $user['password'])) {
                    $data = [
                        'id' => $user['id'],
                        'name_user' => $user['first_name'] . ' ' . $user['last_name'],
                        'username' => $user['username'],
                        'email' => $user['email'],
                        'role_id' => $user['role_id']
                    ];
                    $this->session->set_userdata($data);
                    if ($user['role_id'] == 1) {
                        redirect('developer/dashboard');
                    } else if ($user['role_id'] == 2) {
                        redirect('cms/dashboard');
                    } else if ($user['role_id'] == 3) {
                        redirect('cms/dashboard');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah!</div>');
                    redirect('cms/auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $field . ' belum aktif!</div>');
                redirect('cms/auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">' . $field . ' belum terdaftar!</div>');
            redirect('cms/auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name_user');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');
        $this->session->unset_userdata('id_level');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda telah logout!</div>');
        redirect('cms/auth');
    }

    public function is_logged_in()
    {
        $user = $this->session->userdata('id');
        return isset($user);
    }

    public function users($input = null)
    {
        is_logged_in();
        $data['tab'] = $input;
        $config['web'] = $this->management->config_data()->result_array();
        $data['menu_title'] = "User Management";
        $data['user'] = $this->role->getRoleName();
        $data['role'] = $this->management->get_role()->result_array();
        $data['count'] = $this->member->get_count();
        $data['url'] = "cms/users";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/users', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_users');
    }

    public function add_user_admin()
    {
        $this->form_validation->set_rules('first_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password1', 'Password', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

        $data = [
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'image' => 'default.jpg',
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'role_id' => $this->input->post('role_id'),
            'is_active' => $this->input->post('status_id'),
            'date_created' => time()
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Penambahan data users baru sukses!</div>');
            redirect('cms/users');
        } else {
            redirect('cms/users');
        }
    }

    public function get_data_user_admin()
    {
        is_logged_in();
        $list = $this->users_admin->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->first_name . ' ' . $item->last_name;
            $row[] = $item->username;
            $row[] = $item->email;
            $row[] = '<center><button class="btn-link d-delete" onclick="profile_user_admin(' . "'" . $item->id . "'" . ')">.../profile/' . $item->image . '</button></center>';
            $row[] = $item->role;
            if ($item->is_active == 0) {
                $row[] = '<td align="center"><span class="label label-warning">Not Active</span></td>';
            } else if ($item->is_active == 1) {
                $row[] = '<td align="center"><span class="label label-success">Active</span></td>';
            } else if ($item->is_active == 2) {
                $row[] = '<td align="center"><span class="label label-danger">Block</span></td>';
            }

            // add html for action
            if ($item->role_id == 2 || $item->role_id == 3) {
                $btnDeleteUserAdmin = '<button class="btn-link d-delete" onclick="delete_user_admin(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button>';
                $btnEditUserAdmin = '<button class="btn-link d-edit" onclick="edit_user_admin(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button>';
            } else if ($item->role_id == 1) {
                $btnDeleteUserAdmin = "";
                $btnEditUserAdmin = "";
            }
            $row[] = '<center><button class="btn-link d-view" onclick="view_user_admin(' . "'" . $item->id . "'" . ')" title="Lihat"><i class="fontello-eye-outline" style="color:blue; font-size:15px;"></i></button>' . ' ' . $btnEditUserAdmin . ' ' . $btnDeleteUserAdmin . '</center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->users_admin->count_all(),
            "recordsFiltered" => $this->users_admin->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_user_admin($id)
    {
        is_logged_in();
        $data = $this->users_admin->get_by_id($id);
        echo json_encode($data);
    }

    public function edit_users_admin()
    {
        $id = $this->input->post('edit_id');
        $where = ['id' => $id];
        $pswd = $this->input->post('password');
        $this->form_validation->set_rules('edit_first_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('edit_username', 'Username', 'required|trim');
        $this->form_validation->set_rules('edit_password1', 'Password', 'required|trim');
        $this->form_validation->set_rules('edit_email', 'Email', 'required|trim|valid_email');
        $data = null;
        if ($pswd != null) {
            $data = [
                'first_name' => $this->input->post('edit_first_name'),
                'last_name' => $this->input->post('edit_last_name'),
                'username' => $this->input->post('edit_username'),
                'email' => $this->input->post('edit_email'),
                'password' => password_hash($this->input->post('edit_password1'), PASSWORD_DEFAULT),
                'image' => 'default.jpg',
                'role_id' => $this->input->post('edit_role_id'),
                'is_active' => $this->input->post('edit_status_id'),
                'date_created' => time()
            ];
        } else {
            $data = [
                'first_name' => $this->input->post('edit_first_name'),
                'last_name' => $this->input->post('edit_last_name'),
                'username' => $this->input->post('edit_username'),
                'email' => $this->input->post('edit_email'),
                'image' => 'default.jpg',
                'role_id' => $this->input->post('edit_role_id'),
                'is_active' => $this->input->post('edit_status_id'),
                'date_created' => time()
            ];
        }

        if ($this->form_validation->run() == true) {
            $this->management->edit_users($where, $data, 'cms_user');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data users sukses!</div>');
            redirect('cms/users');
        } else {
            redirect('cms/users');
        }
    }

    public function delete_users_admin($id)
    {
        $this->view_user->delete_user_admin($id);
        echo 'Data harian berhasil dihapus!';
    }

    public function get_data_member()
    {
        is_logged_in();
        $list = $this->member->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->name;
            $row[] = $item->email;
            $row[] = $item->phone;
            $row[] = '<center><button class="btn-link d-delete" onclick="profile_member(' . "'" . $item->id . "'" . ')">.../profile/' . $item->photo . '</button></center>';
            $row[] = $item->nama_paket;
            if ($item->status_paket == 1) {
                $row[] = '<td align="center"><span class="label label-warning">PENDING</span></td>';
            } else if ($item->status_paket == 2) {
                $row[] = '<td align="center"><span class="label label-success">ACTIVE</span></td>';
            } else {
                $row[] = '<td align="center"><span class="label label-danger">INACTIVE</span></td>';
            }
            if ($item->is_active == 0) {
                $row[] = '<td align="center"><span class="label label-warning">Not Active</span></td>';
            } else if ($item->is_active == 1) {
                $row[] = '<td align="center"><span class="label label-success">Active</span></td>';
            } else if ($item->is_active == 2) {
                $row[] = '<td align="center"><span class="label label-danger">Block</span></td>';
            }
            $approve = "";
            if ($item->nama_paket != null && $item->status_paket != 2) {
                $approve = '<button class="btn-link d-view" onclick="approve_member(' . "'" . $item->id . "'" . ')" title="Approve Paket"><i class="fontello-check" style="color:green; font-size:15px;"></i></button>';
            }
            $row[] = '<center>' . $approve . '<button class="btn-link d-view" onclick="view_member(' . "'" . $item->id . "'" . ')" title="Lihat"><i class="fontello-eye-outline" style="color:blue; font-size:15px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->member->count_all(),
            "recordsFiltered" => $this->member->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_data_member($id)
    {
        is_logged_in();
        $data = $this->member->get_by_id($id);
        echo json_encode($data);
    }

    public function approve_member($id)
    {
        $where = array('id' => $id);
        $data = ['status_paket' => 2];
        $this->member->update_approve_member($where, $data, 'cms_user_student');
        // echo 'Data submenu berhasil dihapus!';
    }

    public function footer_settings()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['kiri'] = $this->footer_set->config_data("1");
        $data['tengah'] = $this->footer_set->config_data("2");
        $data['kanan'] = $this->footer_set->config_data("3");
        $data['bawah'] = $this->footer_set->config_data("4");
        $data['menu_title'] = "Footer Settings";
        $data['url'] = "cms/footer_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/footer_settings', $data);
        $this->load->view('templates/footer', $config);
    }

    public function update_footer_kiri()
    {
        $this->form_validation->set_rules('content-kiri', 'Content', 'required|trim');
        $where = ['id' => "1"];
        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("footer_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-kiri'),
            'content' => $this->input->post('content-kiri'),
            'image' => $logo,
            'posisi' => "kiri",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer sukses!</div>');
            redirect('cms/footer_settings');
        } else {
            redirect('cms/footer_settings');
        }
    }

    public function update_footer_tengah()
    {
        $this->form_validation->set_rules('content-tengah', 'Content', 'required|trim');
        $where = ['id' => "2"];
        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-tengah'),
            'content' => $this->input->post('content-tengah'),
            'posisi' => "tengah",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer sukses!</div>');
            redirect('cms/footer_settings');
        } else {
            redirect('cms/footer_settings');
        }
    }

    public function update_footer_kanan()
    {
        $this->form_validation->set_rules('content-kanan', 'Content', 'required|trim');
        $where = ['id' => "3"];
        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-kanan'),
            'content' => $this->input->post('content-kanan'),
            'posisi' => "kanan",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer sukses!</div>');
            redirect('cms/footer_settings');
        } else {
            redirect('cms/footer_settings');
        }
    }

    public function update_footer_bawah()
    {
        $this->form_validation->set_rules('content-bawah', 'Content', 'required|trim');
        $where = ['id' => "4"];
        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-bawah'),
            'content' => $this->input->post('content-bawah'),
            'posisi' => "bawah",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data footer sukses!</div>');
            redirect('cms/footer_settings');
        } else {
            redirect('cms/footer_settings');
        }
    }

    public function header_settings()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu'] = $this->menu->getMenuSettings()->result_array();
        $data['logo'] = $this->footer_set->config_data("5");
        $data['menu_title'] = "Header Settings";
        $data['url'] = "cms/header_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/header_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_header_setting');
    }

    public function update_logo_header()
    {
        $this->form_validation->set_rules('content-header', 'Link', 'required|trim');
        $where = ['id' => "5"];
        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImage($this->input->post('old_logo'));
            $logo = $this->_uploadImage("header_");
        } else {
            $logo = $this->input->post('old_logo_header');
        }

        $data = [
            'judul' => $this->input->post('title-header'),
            'content' => $this->input->post('content-header'),
            'image' => $logo,
            'posisi' => "header",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->header_set->update_header($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data logo header sukses!</div>');
            redirect('cms/header_settings');
        } else {
            redirect('cms/header_settings');
        }
    }

    public function get_data_menu()
    {
        is_logged_in();
        $list = $this->menu->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->menu_name;
            $row[] = $item->link;
            if ($item->flag == "login") {
                $row[] = 'Login';
            } else if ($item->flag == "not_login") {
                $row[] = 'Not Login';
            }
            if ($item->is_active == 0) {
                $row[] = '<td align="center"><span class="label label-warning">Not Active</span></td>';
            } else if ($item->is_active == 1) {
                $row[] = '<td align="center"><span class="label label-success">Active</span></td>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_menu_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_menu_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->menu->count_all(),
            "recordsFiltered" => $this->menu->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function add_menu_setting()
    {
        $this->form_validation->set_rules('menu_name_add', 'Menu', 'required|trim');
        $this->form_validation->set_rules('menu_link_add', 'Link', 'required|trim');
        $this->form_validation->set_rules('status_login_add', 'Status Login', 'required|trim');
        $this->form_validation->set_rules('menu_status_id', 'Status', 'required|trim');

        $data = [
            'menu_name' => $this->input->post('menu_name_add'),
            'link' => $this->input->post('menu_link_add'),
            'flag' => $this->input->post('status_login_add'),
            'is_active' => $this->input->post('menu_status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_menu_settings', $data);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data menu baru sukses!</div>');
            redirect('cms/header_settings');
        } else {
            redirect('cms/header_settings');
        }
    }

    public function edit_data_menu_setting($id)
    {
        is_logged_in();
        $data = $this->menu->get_by_id($id);
        echo json_encode($data);
    }

    public function update_menu_setting()
    {
        $id = $this->input->post('menu_id_edit');
        $where = ['id' => $id];
        $this->form_validation->set_rules('menu_name_edit', 'Menu', 'required|trim');
        $this->form_validation->set_rules('menu_link_edit', 'Link', 'required|trim');
        $this->form_validation->set_rules('status_login_edit', 'Status Login', 'required|trim');
        $this->form_validation->set_rules('menu_status_id', 'Status', 'required|trim');

        $data = [
            'menu_name' => $this->input->post('menu_name_edit'),
            'link' => $this->input->post('menu_link_edit'),
            'flag' => $this->input->post('status_login_edit'),
            'is_active' => $this->input->post('menu_status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->management->edit_users($where, $data, 'cms_menu_settings');
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Perubahan data menu sukses!</div>');
            redirect('cms/header_settings');
        } else {
            redirect('cms/header_settings');
        }
    }

    public function delete_menu_setting($id)
    {
        $this->menu->delete_menu_setting($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function get_data_submenu()
    {
        is_logged_in();
        $list = $this->submenu->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->menu_name;
            $row[] = $item->name_sub;
            $row[] = $item->link;
            if ($item->flag == "login") {
                $row[] = 'Login';
            } else if ($item->flag == "not_login") {
                $row[] = 'Not Login';
            }
            if ($item->is_active == 0) {
                $row[] = '<td align="center"><span class="label label-warning">Not Active</span></td>';
            } else if ($item->is_active == 1) {
                $row[] = '<td align="center"><span class="label label-success">Active</span></td>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_submenu_setting(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_submenu_setting(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->submenu->count_all(),
            "recordsFiltered" => $this->submenu->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function add_submenu_setting()
    {
        $this->form_validation->set_rules('submenu_menu_id_add', 'Menu', 'required|trim');
        $this->form_validation->set_rules('submenu_name_add', 'Submenu', 'required|trim');
        $this->form_validation->set_rules('submenu_link_add', 'Link', 'required|trim');
        $this->form_validation->set_rules('sub_status_login_add', 'Status Login', 'required|trim');
        $this->form_validation->set_rules('submenu_status_id', 'Status', 'required|trim');

        $data = [
            'menu_id' => $this->input->post('submenu_menu_id_add'),
            'name_sub' => $this->input->post('submenu_name_add'),
            'link' => $this->input->post('submenu_link_add'),
            'flag' => $this->input->post('sub_status_login_add'),
            'is_active' => $this->input->post('submenu_status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_submenu_settings', $data);
            $this->session->set_flashdata('message-submenu', '<div class="alert alert-success" role="alert">Penambahan data submenu baru sukses!</div>');
            redirect('cms/header_settings');
        } else {
            redirect('cms/header_settings');
        }
    }

    public function edit_data_submenu_setting($id)
    {
        is_logged_in();
        $data = $this->submenu->get_by_id($id);
        echo json_encode($data);
    }

    public function update_submenu_setting()
    {
        $id = $this->input->post('submenu_id_edit');
        $where = ['id' => $id];
        $this->form_validation->set_rules('submenu_menu_id_edit', 'Menu', 'required|trim');
        $this->form_validation->set_rules('submenu_name_edit', 'Submenu', 'required|trim');
        $this->form_validation->set_rules('submenu_link_edit', 'Link', 'required|trim');
        $this->form_validation->set_rules('sub_status_login_edit', 'Status Login', 'required|trim');
        $this->form_validation->set_rules('submenu_status_id_edit', 'Status', 'required|trim');

        $data = [
            'menu_id' => $this->input->post('submenu_menu_id_edit'),
            'name_sub' => $this->input->post('submenu_name_edit'),
            'link' => $this->input->post('submenu_link_edit'),
            'flag' => $this->input->post('sub_status_login_edit'),
            'is_active' => $this->input->post('submenu_status_id_edit')
        ];

        if ($this->form_validation->run() == true) {
            $this->management->edit_users($where, $data, 'cms_submenu_settings');
            $this->session->set_flashdata('message-submenu', '<div class="alert alert-success" role="alert">Perubahan data submenu sukses!</div>');
            redirect('cms/header_settings');
        } else {
            redirect('cms/header_settings');
        }
    }

    public function delete_submenu_setting($id)
    {
        $this->submenu->delete_submenu_setting($id);
        echo 'Data submenu berhasil dihapus!';
    }

    public function about_settings()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['atas'] = $this->footer_set->config_data("6");
        $data['bawah'] = $this->footer_set->config_data("7");
        $data['menu_title'] = "About Settings";
        $data['url'] = "cms/about_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/about_settings', $data);
        $this->load->view('templates/footer', $config);
    }

    public function update_about_atas()
    {
        $this->form_validation->set_rules('title_atas', 'Title', 'required|trim');
        $this->form_validation->set_rules('content_atas', 'Content', 'required|trim');
        $where = ['id' => "6"];

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImageSingle($this->input->post('old_logo'));
            $logo = $this->_uploadImageSingle("about_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title_atas'),
            'content' => $this->input->post('content_atas'),
            'image' => $logo,
            'posisi' => "atas",
            'is_active' => $this->input->post('status_id'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data menu about sukses!</div>');
            redirect('cms/about_settings');
        } else {
            redirect('cms/about_settings');
        }
    }

    public function update_about_bawah()
    {
        $this->form_validation->set_rules('title_bawah', 'Title', 'required|trim');
        $this->form_validation->set_rules('content_bawah', 'Content', 'required|trim');
        $where = ['id' => "7"];

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImageSingle($this->input->post('old_logo'));
            $logo = $this->_uploadImageSingle("about_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title_bawah'),
            'content' => $this->input->post('content_bawah'),
            'image' => $logo,
            'posisi' => "bawah",
            'is_active' => $this->input->post('status_id'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data menu about sukses!</div>');
            redirect('cms/about_settings');
        } else {
            redirect('cms/about_settings');
        }
    }

    public function add_blog()
    {
        $this->form_validation->set_rules('judul_add', 'Judul', 'required|trim');
        $this->form_validation->set_rules('content_add', 'Content', 'required|trim');

        $data = [
            'title' => $this->input->post('judul_add'),
            'post_text' => $this->input->post('content_add'),
            'post_slug' => $this->slugify($this->input->post('judul_add')),
            'image' => $this->_uploadImageSingle("blog_"),
            'id_creator' => $this->session->userdata('id'),
            'post_tag' => $this->input->post('tag_add'),
            'id_category' => $this->input->post('category_add'),
            'flag' => "blog",
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_blog_post', $data);
            $id_post = $this->db->insert_id();
            $this->addTagBlog($id_post);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data Blog sukses!</div>');
            redirect('cms/postingan_settings');
        } else {
            redirect('cms/postingan_settings');
        }
    }

    public function add_lowongan()
    {
        $this->form_validation->set_rules('lowongan_judul_add', 'Judul', 'required|trim');
        $this->form_validation->set_rules('lowongan_content_add', 'Content', 'required|trim');

        $data = [
            'title' => $this->input->post('lowongan_judul_add'),
            'post_text' => $this->input->post('lowongan_content_add'),
            'post_slug' => $this->slugify($this->input->post('lowongan_judul_add')),
            'image' => $this->_uploadImageSingle("lowongan_"),
            'id_creator' => $this->session->userdata('id'),
            'post_tag' => $this->input->post('lowongan_tag_add'),
            'id_category' => $this->input->post('lowongan_category_add'),
            'flag' => "lowongan",
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_blog_post', $data);
            $id_post = $this->db->insert_id();
            $this->addTagLowongan($id_post);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data Lowongan sukses!</div>');
            redirect('cms/postingan_settings');
        } else {
            redirect('cms/postingan_settings');
        }
    }

    public function addTagBlog($id_post)
    {
        $post_tag = $this->input->post('tag_add');
        $tag = explode(',', $post_tag);
        if (!empty($tag)) {
            foreach ($tag as $value) {
                $data['id_post'] = $id_post;
                $data['tag_name'] = $value;
                $data['flag'] = "blog";
                $this->db->insert('cms_blog_tag', $data);
            }
        }
    }

    public function addTagLowongan($id_post)
    {
        $post_tag = $this->input->post('lowongan_tag_add');
        $tag = explode(',', $post_tag);
        if (!empty($tag)) {
            foreach ($tag as $value) {
                $data['id_post'] = $id_post;
                $data['tag_name'] = $value;
                $data['flag'] = "lowongan";
                $this->db->insert('cms_blog_tag', $data);
            }
        }
    }

    public function slugify($string)
    {
        //remove prepositions
        $preps = array('in', 'at', 'on', 'by', 'into', 'off', 'onto', 'from', 'to', 'with', 'a', 'an', 'the');
        $pattern = '/\b(?:' . join('|', $preps) . ')\b/i';
        $string = preg_replace($pattern, '', $string);

        // replace non letter or digits by -
        $string = preg_replace('~[^\\pL\d]+~u', '-', $string);

        // trim
        $string = trim($string, '-');

        // transliterate
        $string = iconv('utf-8', 'us-ascii//TRANSLIT', $string);

        // lowercase
        $string = strtolower($string);

        // remove unwanted characters
        $string = preg_replace('~[^-\w]+~', '', $string);



        if (empty($string)) {
            return 'n-a';
        }

        return $string;
    }

    public function get_data_blog()
    {
        $base_url = base_url('blog/post/');
        is_logged_in();
        $list = $this->blog->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = '<a href="' . $base_url . $item->post_slug . '" target="_blank">' . word_limiter($item->title, 5) . '</a>';
            $row[] = word_limiter($item->post_text, 10);
            $row[] = $item->image;
            $row[] = $item->first_name . " " . $item->last_name;
            $row[] = $item->post_tag;
            $row[] = $item->category_name;
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_blog(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_blog(' . "'" . $item->id . "'" . ',' . "'" . $item->image . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->blog->count_all(),
            "recordsFiltered" => $this->blog->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function get_data_lowongan()
    {
        $base_url = base_url('lowongan/post/');
        is_logged_in();
        $list = $this->lowongan->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = '<a href="' . $base_url . $item->post_slug . '" target="_blank">' . word_limiter($item->title, 5) . '</a>';
            $row[] = word_limiter($item->post_text, 10);
            $row[] = $item->image;
            $row[] = $item->first_name . " " . $item->last_name;
            $row[] = $item->post_tag;
            $row[] = $item->category_name;
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_lowongan(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_lowongan(' . "'" . $item->id . "'" . ',' . "'" . $item->image . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->lowongan->count_all(),
            "recordsFiltered" => $this->lowongan->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_blog()
    {
        $id = $this->input->post('blog_edit_id');
        $where = ['id' => $id];
        $this->form_validation->set_rules('judul_edit', 'Judul', 'required|trim');
        $this->form_validation->set_rules('content_edit', 'Content', 'required|trim');

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImageSingle($this->input->post('old_logo'));
            $logo = $this->_uploadImageSingle("blog_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'title' => $this->input->post('judul_edit'),
            'post_text' => $this->input->post('content_edit'),
            'post_slug' => $this->slugify($this->input->post('judul_edit')),
            'image' => $logo,
            'id_creator' => $this->session->userdata('id'),
            'post_tag' => $this->input->post('tag_edit'),
            'id_category' => $this->input->post('category_edit'),
        ];

        if (!empty($post_tag)) {
            foreach ($post_tag as $key => $value) {
                $data['id_post'] = $id;
                $data['tag_name'] = $value;
                $data['flag'] = "blog";
                $this->db->insert('cms_blog_tag', $data);
            }
        }

        if ($this->form_validation->run() == true) {
            $this->management->edit_users($where, $data, 'cms_blog_post');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data blog sukses!</div>');
            redirect('cms/postingan_settings');
        } else {
            redirect('cms/postingan_settings');
        }
    }

    public function edit_lowongan()
    {
        $id = $this->input->post('lowongan_edit_id');
        $where = ['id' => $id];
        $this->form_validation->set_rules('lowongan_judul_edit', 'Judul', 'required|trim');
        $this->form_validation->set_rules('lowongan_content_edit', 'Content', 'required|trim');

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImageSingle($this->input->post('lowongan_old_logo'));
            $logo = $this->_uploadImageSingle("lowongan_");
        } else {
            $logo = $this->input->post('lowongan_old_logo');
        }

        $data = [
            'title' => $this->input->post('lowongan_judul_edit'),
            'post_text' => $this->input->post('lowongan_content_edit'),
            'post_slug' => $this->slugify($this->input->post('lowongan_judul_edit')),
            'image' => $logo,
            'id_creator' => $this->session->userdata('id'),
            'post_tag' => $this->input->post('lowongan_tag_edit'),
            'id_category' => $this->input->post('lowongan_category_edit'),
        ];

        if (!empty($post_tag)) {
            foreach ($post_tag as $key => $value) {
                $data['id_post'] = $id;
                $data['tag_name'] = $value;
                $data['flag'] = "lowongan";
                $this->db->insert('cms_blog_tag', $data);
            }
        }

        if ($this->form_validation->run() == true) {
            $this->management->edit_users($where, $data, 'cms_blog_post');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data lowongan sukses!</div>');
            redirect('cms/postingan_settings');
        } else {
            redirect('cms/postingan_settings');
        }
    }

    public function delete_blog($id, $image)
    {
        $this->blog->delete_blog($id);
        if ($image != "" && $image != null && $image != "default.png") {
            $this->_deleteImageSingle($image);
        }
        echo 'Data harian berhasil dihapus!';
    }

    public function delete_lowongan($id, $image)
    {
        $this->lowongan->delete_lowongan($id);
        if ($image != "" && $image != null && $image != "default.png") {
            $this->_deleteImageSingle($image);
        }
        echo 'Data harian berhasil dihapus!';
    }

    public function edit_data_blog($id)
    {
        is_logged_in();
        $data = $this->blog->get_by_id($id);
        echo json_encode($data);
    }

    public function edit_data_lowongan($id)
    {
        is_logged_in();
        $data = $this->blog->get_by_id($id);
        echo json_encode($data);
    }

    private function _uploadImage($param)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('His');

        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/profile/";
        $config['file_name'] = "logo_" . $param . $now;
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("logo")) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            print_r($error);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            return $upload['upload_data']['file_name'];
        }

        return "default.png";
    }

    private function _deleteImage($file)
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/logo/" . $file);
    }

    private function _uploadImageSingle($param)
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date('His');

        $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/single/";
        $config['file_name'] = "image_" . $param . $now;
        $config['allowed_types'] = 'jpg|png|jpeg';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("logo")) {
            $error = $this->upload->display_errors();
            // menampilkan pesan error
            print_r($error);
        } else {
            $upload = array('upload_data' => $this->upload->data());
            return $upload['upload_data']['file_name'];
        }

        return "default.png";
    }

    private function _deleteImageSingle($file)
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/single/" . $file);
    }

    public function postingan_settings($input = null)
    {
        is_logged_in();
        $data['tab'] = $input;
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu_title'] = "Postingan Settings";
        $data['url'] = "cms/postingan_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/postingan_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_postingan_settings');
    }

    public function paket_settings()
    {
        is_logged_in();
        $data['kiri'] = $this->footer_set->config_data("8");
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu_title'] = "Paket Settings";
        $data['url'] = "cms/paket_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/paket_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_paket_settings');
    }

    public function update_paket_kiri()
    {
        $where = ['id' => "8"];

        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-kiri'),
            'content' => $this->input->post('content-kiri'),
            'link' => $this->input->post('link-kiri'),
            'posisi' => "kiri",
            'updated_date' => @mdate($format)
        ];
        $this->footer_set->update_footer($where, $data, 'cms_settings');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data paket sukses!</div>');
        redirect('cms/paket_settings');
    }

    public function get_data_paket()
    {
        is_logged_in();
        $list = $this->paket->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama_paket;
            $row[] = $item->keterangan;
            $row[] = $this->rupiah($item->harga_paket);
            $row[] = $item->icon;
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_paket(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_paket(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->menu->count_all(),
            "recordsFiltered" => $this->menu->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    function rupiah($angka)
    {
        $hasil_rupiah = "Rp. " . number_format($angka, 2, ',', '.');
        return $hasil_rupiah;
    }

    public function add_paket()
    {
        $this->form_validation->set_rules('paket_name_add', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('description_add', 'Keterangan', 'required|trim');
        $this->form_validation->set_rules('harga_paket_add', 'Harga Paket Paket', 'required|trim');
        $this->form_validation->set_rules('icon_add', 'Icon', 'required|trim');

        $data = [
            'nama_paket' => $this->input->post('paket_name_add'),
            'keterangan' => $this->input->post('description_add'),
            'harga_paket' => $this->input->post('harga_paket_add'),
            'icon' => $this->input->post('icon_add')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_paket_settings', $data);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data paket sukses!</div>');
            redirect('cms/paket_settings');
        } else {
            redirect('cms/paket_settings');
        }
    }

    public function edit_data_paket($id)
    {
        is_logged_in();
        $data = $this->paket->get_by_id($id);
        echo json_encode($data);
    }

    public function edit_paket()
    {
        $id = $this->input->post('paket_id_edit');
        $where = ['id' => $id];

        $this->form_validation->set_rules('paket_name_edit', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('description_edit', 'Keterangan', 'required|trim');
        $this->form_validation->set_rules('harga_paket_edit', 'Harga Paket', 'required|trim');
        $this->form_validation->set_rules('icon_edit', 'Icon', 'required|trim');

        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'nama_paket' => $this->input->post('paket_name_edit'),
            'keterangan' => $this->input->post('description_edit'),
            'harga_paket' => $this->input->post('harga_paket_edit'),
            'icon' => $this->input->post('icon_edit'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->paket->edit_paket($where, $data, 'cms_paket_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data paket sukses!</div>');
            redirect('cms/paket_settings');
        } else {
            redirect('cms/paket_settings');
        }
    }

    public function delete_paket($id)
    {
        $this->paket->delete_paket($id);
        echo 'Data harian berhasil dihapus!';
    }

    public function pdf_settings()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu_title'] = "PDF Settings";
        $data['url'] = "cms/pdf_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/pdf_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_pdf_settings');
    }

    public function add_file_pdf()
    {
        $this->form_validation->set_rules('judul_add', 'Judul', 'required|trim');
        $this->form_validation->set_rules('file_pdf_add', 'File PDF', 'required|trim');
        $this->form_validation->set_rules('paket_add', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('status_id', 'Status', 'required|trim');

        $data = [
            'judul' => $this->input->post('judul_add'),
            'file_name' => $this->input->post('file_pdf_add'),
            'file_slug' => $this->slugify($this->input->post('judul_add')),
            'kategori' => $this->input->post('kategori_add'),
            'id_paket' => $this->input->post('paket_add'),
            'id_creator' => $this->session->userdata('id'),
            'is_active' => $this->input->post('status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_pdf_settings', $data);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data PDF sukses!</div>');
            redirect('cms/pdf_settings');
        } else {
            redirect('cms/pdf_settings');
        }
    }

    public function get_data_pdf()
    {
        is_logged_in();
        $list = $this->pdf->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->judul;
            $row[] = '<a href="#" onclick="return show_pdf(' . "'" . $item->id . "'" . ')">' . $item->file_name . '</a>';
            $row[] = $item->kategori;
            $row[] = $item->nama_paket;
            $row[] = $item->first_name . ' ' . $item->last_name;
            if ($item->is_active == 0) {
                $row[] = '<center><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_file_pdf(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button><button class="btn-link d-delete" onclick="delete_file_pdf(' . "'" . $item->id . "'" . ')" title="Hapus"><i class="fontello-trash-2" style="color:red; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->pdf->count_all(),
            "recordsFiltered" => $this->pdf->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function edit_file_pdf()
    {
        $id = $this->input->post('file_pdf_id_edit');
        $where = ['id' => $id];

        $this->form_validation->set_rules('judul_edit', 'Judul', 'required|trim');
        $this->form_validation->set_rules('file_pdf_edit', 'File PDF', 'required|trim');
        $this->form_validation->set_rules('paket_edit', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('status_id', 'Status', 'required|trim');

        //load date helper
        $this->load->helper('date');
        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('judul_edit'),
            'file_name' => $this->input->post('file_pdf_edit'),
            'file_slug' => $this->slugify($this->input->post('judul_edit')),
            'kategori' => $this->input->post('kategori_edit'),
            'id_paket' => $this->input->post('paket_edit'),
            'id_creator' => $this->session->userdata('id'),
            'is_active' => $this->input->post('status_id'),
            'updated_date' => @mdate($format)
        ];


        if ($this->form_validation->run() == true) {
            $this->pdf->edit_pdf($where, $data, 'cms_pdf_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data pdf sukses!</div>');
            redirect('cms/pdf_settings');
        } else {
            redirect('cms/pdf_settings');
        }
    }

    public function edit_data_file_pdf($id)
    {
        is_logged_in();
        $data = $this->pdf->get_by_id($id);
        echo json_encode($data);
    }

    public function delete_file_pdf($id)
    {
        $this->pdf->delete_pdf($id);
        echo 'Data PDF berhasil dihapus!';
    }

    public function paket_data()
    {
        $data = $this->db->get('cms_paket_settings')->result_array();
        echo json_encode(array('result' => $data));
    }

    public function kategori_blog_data()
    {
        $data = $this->db->get_where('cms_blog_category', array('flag' => 'blog'))->result_array();
        echo json_encode(array('result' => $data));
    }

    public function kategori_lowongan_data()
    {
        $data = $this->db->get_where('cms_blog_category', array('flag' => 'lowongan'))->result_array();
        echo json_encode(array('result' => $data));
    }

    public function add_kategori_blog($kategori = null)
    {
        $data = [
            'category_name' => $kategori,
            'flag' => 'blog'
        ];

        $this->db->where('category_name', $kategori);
        $query = $this->db->get('cms_blog_category');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return false;
        } else {
            $this->db->insert('cms_blog_category', $data);
            echo 'Data kategori berhasil simpan!';
        }
    }

    public function add_kategori_lowongan($kategori = null)
    {
        $data = [
            'category_name' => $kategori,
            'flag' => 'lowongan'
        ];

        $this->db->where('category_name', $kategori);
        $query = $this->db->get('cms_blog_category');
        $count_row = $query->num_rows();

        if ($count_row > 0) {
            return false;
        } else {
            $this->db->insert('cms_blog_category', $data);
            echo 'Data kategori berhasil simpan!';
        }
    }

    public function promotion_settings()
    {
        is_logged_in();
        $data['atas'] = $this->footer_set->config_data("8");
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['menu_title'] = "Promotion Settings";
        $data['url'] = "cms/promotion_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/promotion_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_promotion_settings');
    }

    public function get_data_testimoni()
    {
        is_logged_in();
        $list = $this->testimoni->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $no . ".";
            $row[] = $item->nama;
            $row[] = $item->profesi;
            $row[] = $item->komentar;
            $row[] = $item->photo;
            if ($item->is_active == 0) {
                $row[] = '<center><td align="center"><span class="label label-warning">Not Active</span></td></center>';
            } else if ($item->is_active == 1) {
                $row[] = '<center><td align="center"><span class="label label-success">Active</span></td></center>';
            }
            $row[] = '<center><button class="btn-link d-edit" onclick="edit_testimoni(' . "'" . $item->id . "'" . ')" title="Edit"><i class="fa fa-edit" style="color:green; font-size:12px;"></i></button></center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->pdf->count_all(),
            "recordsFiltered" => $this->pdf->count_filtered(),
            "data" => $data,
        );
        // output to json format
        echo json_encode($output);
    }

    public function add_testimoni()
    {
        $this->form_validation->set_rules('nama_member_add', 'Judul', 'required|trim');
        $this->form_validation->set_rules('profesi_add', 'File PDF', 'required|trim');
        $this->form_validation->set_rules('komentar_add', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('status_id', 'Status', 'required|trim');

        $data = [
            'nama' => $this->input->post('nama_member_add'),
            'profesi' => $this->input->post('profesi_add'),
            'komentar' => $this->input->post('komentar_add'),
            'photo' => $this->_uploadImageSingle("member_testi_"),
            'is_active' => $this->input->post('status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->db->insert('cms_testimonial_settings', $data);
            $this->session->set_flashdata('message-menu', '<div class="alert alert-success" role="alert">Penambahan data testimonial sukses!</div>');
            redirect('cms/promotion_settings');
        } else {
            redirect('cms/promotion_settings');
        }
    }

    public function edit_testimoni()
    {
        $id = $this->input->post('testimoni_id_edit');
        $where = ['id' => $id];

        $this->form_validation->set_rules('nama_member_edit', 'Judul', 'required|trim');
        $this->form_validation->set_rules('profesi_edit', 'File PDF', 'required|trim');
        $this->form_validation->set_rules('komentar_edit', 'Nama Paket', 'required|trim');
        $this->form_validation->set_rules('status_id', 'Status', 'required|trim');

        if (!empty($_FILES["logo"]["name"])) {
            $this->_deleteImageSingle($this->input->post('old_logo'));
            $logo = $this->_uploadImageSingle("member_testi_");
        } else {
            $logo = $this->input->post('old_logo');
        }

        $data = [
            'nama' => $this->input->post('nama_member_edit'),
            'profesi' => $this->input->post('profesi_edit'),
            'komentar' => $this->input->post('komentar_edit'),
            'photo' => $logo,
            'is_active' => $this->input->post('status_id')
        ];

        if ($this->form_validation->run() == true) {
            $this->pdf->edit_pdf($where, $data, 'cms_testimonial_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data testimoni sukses!</div>');
            redirect('cms/promotion_settings');
        } else {
            redirect('cms/promotion_settings');
        }
    }

    public function edit_data_testimoni($id)
    {
        is_logged_in();
        $data = $this->testimoni->get_by_id($id);
        echo json_encode($data);
    }

    public function cara_bayar_settings()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['cara_bayar'] = $this->footer_set->config_data("9");
        $data['menu_title'] = "Cara Bayar Settings";
        $data['url'] = "cms/cara_bayar_settings";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/cara_bayar_settings', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_cara_bayar', $config);
    }

    public function update_cara_bayar()
    {
        $this->form_validation->set_rules('content-bayar', 'Content', 'required|trim');
        $where = ['id' => "9"];

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('title-bayar'),
            'content' => $this->input->post('content-bayar'),
            'posisi' => "tengah",
            'link' => $this->input->post('link-bayar'),
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data cara bayar sukses!</div>');
            redirect('cms/cara_bayar_settings');
        } else {
            redirect('cms/cara_bayar_settings');
        }
    }

    public function konfigurasi_email()
    {
        is_logged_in();
        $config['web'] = $this->view_user->config_data()->result_array();
        $data['config_email'] = $this->footer_set->config_data("10");
        $data['cara_bayar'] = $this->footer_set->config_data("11");
        $data['upgrade_paket'] = $this->footer_set->config_data("12");
        $data['menu_title'] = "Konfigurasi Email";
        $data['url'] = "cms/konfigurasi_email";
        $this->load->view('templates/header', $config);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/breadcumb', $data);
        $this->load->view('cms/konfigurasi_email', $data);
        $this->load->view('templates/footer', $config);
        $this->load->view('templates/script_config_email', $config);
    }

    public function update_konfigurasi_email()
    {
        $this->form_validation->set_rules('message_email', 'Message', 'required|trim');
        $where = ['id' => "10"];

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('subject_email'),
            'content' => $this->input->post('message_email'),
            'posisi' => "tengah",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan konfigurasi email sukses!</div>');
            redirect('cms/konfigurasi_email');
        } else {
            redirect('cms/konfigurasi_email');
        }
    }

    public function update_email_cara_bayar()
    {
        $this->form_validation->set_rules('message_cara_bayar', 'Content', 'required|trim');
        $where = ['id' => "11"];

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('subject_cara_bayar'),
            'content' => $this->input->post('message_cara_bayar'),
            'posisi' => "tengah",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan data cara bayar sukses!</div>');
            redirect('cms/konfigurasi_email');
        } else {
            redirect('cms/konfigurasi_email');
        }
    }

    public function update_upgrade_paket()
    {
        $this->form_validation->set_rules('message_upgrade_email', 'Content', 'required|trim');
        $where = ['id' => "12"];

        //load date helper
        $this->load->helper('date');

        $format = "%Y-%m-%d %h:%i";

        $data = [
            'judul' => $this->input->post('subject_upgrade_email'),
            'content' => $this->input->post('message_upgrade_email'),
            'posisi' => "tengah",
            'updated_date' => @mdate($format)
        ];

        if ($this->form_validation->run() == true) {
            $this->footer_set->update_footer($where, $data, 'cms_settings');
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Perubahan pesan email data upgrade paket sukses!</div>');
            redirect('cms/konfigurasi_email');
        } else {
            redirect('cms/konfigurasi_email');
        }
    }

    public function send_verification($id)
    {
        $config_email = $this->frontend->config_email_konfirmasi();
        $get_user = $this->frontend->get_data_user($id);
        $get_paket = $this->frontend->get_data_paket($get_user->paket);
        $before_replace = array('$nama', '$email', '$phone', '$paket_name');
        $after_replace = array($get_user->name, $get_user->email, $get_user->phone, $get_paket->nama_paket);
        $from = $this->config->item('smtp_user');
        $to = $get_user->email;
        $subject = $config_email->judul;
        $message = str_replace($before_replace, $after_replace, $config_email->content);

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            echo 'Your Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }
}
