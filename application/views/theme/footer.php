</main>
<!-- footer part start-->
<footer class="footer-area section-bg" data-background="<?= base_url('assets/consultingbiz/img/gallery/footer_bg.jpg'); ?>" style="background-image: url('<?= base_url('assets/consultingbiz/img/gallery/footer_bg.jpg'); ?>');">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="single-footer-widget footer_1">
                    <?php foreach ($footer_kiri as $fkiri) : ?>
                        <?php if ($fkiri['judul'] != null) {
                            echo "<h4>" . $fkiri['judul'] . "</h4>";
                        } ?>
                        <a href="?"> <img src="<?= base_url('assets/uploads/logo/'); ?><?= $fkiri['image']; ?>" height="80px"> </a>
                        <?= $fkiri['content']; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-4">
                <div class="single-footer-widget footer_2">
                    <?php foreach ($footer_tengah as $ftengah) : ?>
                        <?php if ($ftengah['judul'] != null) {
                            echo "<h4>" . $ftengah['judul'] . "</h4>";
                        } ?>
                        <?= $ftengah['content']; ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-md-4">
                <div class="single-footer-widget footer_3">
                    <?php foreach ($footer_kanan as $fkanan) : ?>
                        <?php if ($fkanan['judul'] != null) {
                            echo "<h4>" . $fkanan['judul'] . "</h4>";
                        } ?>
                        <?= $fkanan['content']; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright_part_text text-center">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="footer-text m-0" style="color: #ffffff;">
                                <?php foreach ($footer_bawah as $fbawah) : ?>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy; <script>
                                        document.write(new Date().getFullYear());
                                    </script> <?= $fbawah['content']; ?>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                <?php endforeach; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer part end-->

<!-- jquery plugins here-->
<!-- jquery -->
<script src="<?= base_url('assets/front/js/jquery-1.12.1.min.js'); ?>"></script>
<!-- popper js -->
<script src="<?= base_url('assets/front/js/popper.min.js'); ?>"></script>
<!-- bootstrap js -->
<script src="<?= base_url('assets/front/js/bootstrap.min.js'); ?>"></script>
<!-- easing js -->
<script src="<?= base_url('assets/front/js/jquery.magnific-popup.js'); ?>"></script>
<!-- swiper js -->
<script src="<?= base_url('assets/front/js/swiper.min.js'); ?>"></script>
<!-- swiper js -->
<script src="<?= base_url('assets/front/js/masonry.pkgd.js'); ?>"></script>
<!-- particles js -->
<script src="<?= base_url('assets/front/js/owl.carousel.min.js'); ?>"></script>
<script src="front/js/jquery.nice-select.min.js'); ?>"></script>
<!-- swiper js -->
<script src="<?= base_url('assets/front/js/jquery.slicknav.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/js/slick.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/js/jquery.counterup.min.js'); ?>"></script>
<script src="<?= base_url('assets/front/js/waypoints.min.js'); ?>"></script>
<!-- custom js -->
<script src="<?= base_url('assets/front/js/custom.js'); ?>"></script>
<!-- notify js -->
<script src="<?= base_url('assets/front/js/jnoty.js'); ?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

<script type="text/javascript">
    (function($) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $('a[data-toggle="tab"]').removeClass('btn-primary');
            $('a[data-toggle="tab"]').addClass('btn-default');
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
        });

        // Brand
        $('.brand-active').slick({
            dots: false,
            infinite: true,
            autoplay: true,
            speed: 400,
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },

                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        var carousel = function() {
            $('.carousel-testimony').owlCarousel({
                center: true,
                loop: true,
                autoplay: true,
                autoplaySpeed: 2000,
                items: 1,
                margin: 30,
                stagePadding: 0,
                nav: false,
                navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            });

        };
        carousel();
    })(jQuery);
    $("#alert-success").fadeTo(5000, 500).slideUp(500, function() {
        $("#alert-success").slideUp(500);
    });

    function openConfirm() {
        $('#confirm').modal('show');
        $('#resetpassword').modal('toggle');
    }

    function logout() {
        window.location.href = "<?= base_url('member/logout'); ?>";
    }

    function mainSlider() {
        var BasicSlider = $('.slider-active');
        BasicSlider.on('init', function(e, slick) {
            var $firstAnimatingElements = $('.single-slider:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        BasicSlider.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('.single-slider[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        BasicSlider.slick({
            autoplay: false,
            autoplaySpeed: 10000,
            dots: false,
            fade: true,
            arrows: true,
            prevArrow: '<button type="button" class="btn_4" style="backround-color: red;"><i class="ti-arrow-left"></i></button>',
            nextArrow: '<button type="button" class="btn_4"><i class="ti-arrow-right"></i></button>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });

        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
    }
    mainSlider();
</script>
</body>

</html>