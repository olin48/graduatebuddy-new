<div class="col-lg-4">
    <div class="blog_right_sidebar">
        <aside class="single_sidebar_widget search_widget">
            <form action="<?= base_url('lowongan/search'); ?>" method="post">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="search_post" name="search_post" placeholder='Search Keyword' onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                        <div class="input-group-append">
                            <button class="btn post-btn" style="background-color: #f44a40" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <button class="btn post-btn" style="width: 100%;" type="submit">Search</button>
            </form>
        </aside>
        <aside class="single_sidebar_widget post_category_widget">
            <h4 class="widget_title">Category</h4>
            <ul class="list cat-list">
                <?php foreach ($get_category as $gc) : ?>
                    <li>
                        <a href="<?= base_url('lowongan/category/'); ?><?= $gc['category_name']; ?>" class="d-flex">
                            <p><?= $gc['category_name']; ?></p>
                            <p>(<?= $gc['count_category']; ?>)</p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </aside>
        <aside class="single_sidebar_widget popular_post_widget">
            <h3 class="widget_title">Recent Post</h3>
            <?php foreach ($recent_post as $rp) : ?>
                <div class="media post_item">
                    <img src="<?= base_url('assets/uploads/single/'); ?><?= $rp['image']; ?>" width="125px;" alt="post">
                    <div class="media-body">
                        <a href="<?= base_url('lowongan/post/'); ?><?= $rp['post_slug']; ?>">
                            <h3><?php echo word_limiter($rp['title'], 5); ?></h3>
                        </a>
                        <p><?= date('d F Y', strtotime($rp['created_date'])); ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </aside>
        <aside class="single_sidebar_widget tag_cloud_widget">
            <h4 class="widget_title">Tag Clouds</h4>
            <ul class="list">
                <?php foreach ($tag_post as $tag) : ?>
                    <li>
                        <a href="<?= base_url('lowongan/tag/'); ?><?= $tag['tag_name']; ?>"><?= $tag['tag_name']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </aside>
    </div>
</div>
</div>
</div>
</section>
<!--================Blog Area end =================-->