<script type="text/javascript">
    (function($) {

    })(jQuery);

    function showPDFPsikotest(id) {
        $('#pdf_show_psikotest').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_file_pdf') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#pdf_show_psikotest').append("<iframe frameborder='0' width='100%' height='500' src='<?= base_url('pdf/'); ?>" + data.file_name + "/' scrolling='no' marginwidth='0' marginheight='0'></iframe>");
                // $('#show_file_pdf').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('View Photo Profile Admin'); // Set title to Bootstrap modal title
            }
        });
    }

    function showPDFWawancara(id) {
        $('#pdf_show_wawancara').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_file_pdf') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#pdf_show_wawancara').append("<iframe frameborder='0' width='100%' height='500' src='<?= base_url('pdf/'); ?>" + data.file_name + "/' scrolling='no' marginwidth='0' marginheight='0'></iframe>");
                // $('#show_file_pdf').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('View Photo Profile Admin'); // Set title to Bootstrap modal title
            }
        });
    }
</script>