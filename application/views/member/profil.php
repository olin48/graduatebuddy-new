<!--================Blog Area =================-->
<section class="feature_part">
    <div class="container">
        <div class="row justify-content-center" style="padding-top: 120px;">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h2>Profile</h2>
                </div>
            </div>
        </div>

        <?= $this->session->flashdata('message_edit_profile'); ?>
        <?php foreach ($data_profil as $dp) : ?>
            <?php foreach ($paket_name as $pn) : ?>
                <?php if ($dp['paket'] != 0 && $dp['status_paket'] == 2) {
                    echo "<div class=\"alert alert-success\" role=\"alert\" style=\"text-align: center;\">
                Paket anda saat ini adalah <strong>" . $pn['nama_paket'] . "</strong> jika ingin merubahnya silahkan klik button dibawah.<br />
                <a href='" . base_url('member/paket') . "' class=\"btn post-btn\" style=\"margin-top: 15px;\">Upgrade Paket</a>
            </div>";
                } ?>
            <?php endforeach; ?>
            <?php if ($dp['paket'] != 0 && $dp['status_paket'] == 1) {
                echo "<div class=\"alert alert-danger\" role=\"alert\" style=\"text-align: center;\">
                Selesaikan pembayaran paket yang sudah dipilih, klik button dibawah untuk petunjuk pembayaran. <br />
                <button data-toggle=\"modal\" data-target=\"#cara_bayar\" class=\"btn post-btn\" style=\"margin-top: 15px;\">Cara Bayar</button>
            </div>";
            } ?>
            <?php if ($dp['paket'] != 0 && $dp['status_paket'] == 2) {
                echo form_open_multipart('member/edit_profil');
            } else {
                echo form_open_multipart('member/verify_paket');
            } ?>
            <div class="row" style="padding-bottom: 120px;">
                <div class="col-sm-6 col-xl-3">
                    <img src="<?php if ($dp['photo'] == null || $dp['photo'] == "default.jpg") {
                                    echo base_url('assets/uploads/profile/default.jpg');
                                } else {
                                    echo base_url('assets/uploads/profile/' . $dp['photo']);
                                } ?>" width="250px" height="250px">
                    <div class="form-group" style="padding-top: 15px;">
                        <input type="file" class="form-control-file" id="logo" name="logo">
                        <input type="hidden" id="old_logo" name="old_logo" value="<?= $dp['photo']; ?>" />
                        <i style="font-size: 11px;">Maksimal file upload 2MB, dengan format .jpg, .png dan .jpeg</i><br>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="form-group">
                        <label for="usr">Nama :</label>
                        <input type="text" class="form-control" id="full_name" name="full_name" value="<?= $dp['name']; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="usr">Email :</label>
                        <input type="text" class="form-control" id="email" name="email" value="<?= $dp['email']; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="usr">No Handphone :</label>
                        <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= $dp['phone']; ?>" disabled>
                    </div>
                   
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="form-group">
                        <label for="usr">Umur :</label>
                        <input type="text" class="form-control" id="umur" name="umur" value="<?= $dp['umur']; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="usr">Kota Domisili :</label>
                        <input type="text" class="form-control" id="universitas" name="universitas" value="<?= $dp['universitas_name']; ?>" required>
                    </div>
					<div class="form-group">
                        <label for="usr">Universitas :</label>
                        <input type="text" class="form-control" id="universitas" name="universitas" value="<?= $dp['universitas_name']; ?>" required>
                    </div>
                    <button type="submit" class="btn post-btn" style="position: absolute; margin-right: 0px;">Simpan</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        <?php endforeach; ?>
    </div>

    <div id="cara_bayar" class="modal fade" role="dialog">
        <div class="modal-dialog" style="margin: absolut; top: 15%; padding: 10px;">

            <!-- Modal content-->
            <div class="modal-content">
                <form role="form" method="post" action="<?= base_url($cara_bayar->link); ?>">
                    <div class="modal-header">
                        <h4 class="modal-title"><?= $cara_bayar->judul; ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <?= $cara_bayar->content; ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn post-btn" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>