<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <!-- <h2>Service</h2> 
						<p style="align: left"><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>Service</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--::blog_part start::-->
<section class="blog_part section_padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div class="section_tittle text-center">
                    <h2>Selamat Datang, <font style="color:navy"><?= $welcome_name ?></font>
                    </h2>
                </div>
            </div>
        </div>
        <?php foreach ($data_profil as $dp) : ?>
            <?php foreach ($paket_name as $pn) : ?>
                <?php if ($dp['paket'] != 0 && $dp['status_paket'] == 2) {
                    echo "<div class=\"alert alert-success\" role=\"alert\" style=\"text-align: center;\">
                Paket anda saat ini adalah <strong>" . $pn['nama_paket'] . "</strong>. Upgrade ke paket yang lebih lengkap Sekarang.<br />
                <a href='" . base_url('member/paket') . "' class=\"btn post-btn\" style=\"margin-top: 15px;\">Upgrade Paket</a>
            </div>";
                } ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <div class="row justify-content-center" style="padding-top: 50px;">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <h3>Member Area</h3>
                </div>
            </div>
        </div>
        <ul class="nav" style="display: flex; justify-content: center;padding-top: 20px;">
            <li style="padding-right: 20px;"><a class="btn_2" data-toggle="tab" href="#psikotest">Psikotest</a></li>
            <li><a class="btn_2" data-toggle="tab" href="#wawancara">Wawancara</a></li>
        </ul>
        <br />
        <div class="tab-content">
            <div id="psikotest" class="tab-pane fade in active">
                <div style="display: flex; justify-content: center;padding-top: 20px;">
                    <form action="frontend/download_cv_template" method="post">
                        <?= $this->session->flashdata('message_download_cv_template'); ?>
                        <p style="vertical-align: middle;">Silahkan Unduh Workbook dengan klik tombol berikut :
                            <button type="submit" class="btn post-btn py-3 px-2">Download Workbook</button>
                        </p>
                    </form>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xl-4">
                        <?php foreach ($file_pdf_psikotest as $fdp) : ?>
                            <button type="button" class="btn hero-btn" onclick="showPDFPsikotest('<?= $fdp['id']; ?>')" style="text-align: left; width:100%">
                                <i class="fa fa-file-pdf-o"></i> <?= $fdp['judul']; ?>
                            </button>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-6 col-xl-8">
                        <span id="pdf_show_psikotest"></span>
                    </div>
                </div>
                </br></br>

            </div>

            <div id="wawancara" class="tab-pane fade in">
                <div class="row">
                    <div class="col-sm-6 col-xl-4">
                        <?php foreach ($file_pdf_wawancara as $fdw) : ?>
                            <button type="button" class="btn hero-btn" onclick="showPDFWawancara('<?= $fdw['id']; ?>')" style="text-align: left; width:100%">
                                <i class="fa fa-file-pdf-o"></i> <?= $fdw['judul']; ?>
                            </button>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-6 col-xl-8">
                        <span id="pdf_show_wawancara"></span>
                    </div>
                </div>
                </br></br>
            </div>
        </div>
        <!--
        <div class="row justify-content-center" style="padding-top: 80px;">
            <div class="col-xl-5">
                <div class="section_tittle text-center">
                    <p>Berita</p>
                    <h2>Lowongan Kerja</h2>
                </div>
            </div>
        </div>
        
		-->
    </div>
</section>
<!--::blog_part end::-->