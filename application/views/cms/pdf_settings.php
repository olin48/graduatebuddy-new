<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">

    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">

                        <span class="box-btn" data-widget="collapse"><i class="fa fa-minus"></i>
                        </span>
                    </div>
                    <h3 class="box-title"><i class="fontello-doc"></i>
                        <span>Data <?php echo $menu_title; ?></span>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <?= $this->session->flashdata('message'); ?>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_file_pdf">Tambah File PDF</button>
                    <br /><br />
                    <table id="dataFilePDF" class="table table-bordered table-striped" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 5%;">#</th>
                                <th>Judul</th>
                                <th>File PDF</th>
                                <th>Kategori</th>
                                <th>Paket</th>
                                <th>Creator</th>
                                <th>Status</th>
                                <th style="width: 10%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.box -->
            </div>
        </div>

        <!-- Modal -->
        <div id="add_file_pdf" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah File PDF</h4>
                    </div>
                    <?php echo form_open_multipart('cms/add_file_pdf'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="text" class="form-control" id="judul_add" name="judul_add" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">File PDF :</label>
                            <input type="text" class="form-control" id="file_pdf_add" name="file_pdf_add" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kategori :</label>
                            <select name="kategori_add" id="kategori_add" class="form-control" required>
                                <option value="">Pilih Kategori</option>
                                <option value="psikotest">Psikotes</option>
                                <option value="wawancara">Wawancara</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Nama Paket :</label>
                            <select name="paket_add" id="paket_add" class="selectpicker" data-live-search="true" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="status_id" id="status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="show_file_pdf" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">View <?php echo $menu_title; ?></h4>
                    </div>
                    <div style="text-align: center; margin-top:50px; margin-bottom:50px;">
                        <span id="file_pdf"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                    </>
                </div>
            </div>
        </div>

        <div id="edit_file_pdf" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit File PDF</h4>
                    </div>
                    <?php echo form_open_multipart('cms/edit_file_pdf'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Judul :</label>
                            <input type="hidden" class="form-control" id="file_pdf_id_edit" name="file_pdf_id_edit" required>
                            <input type="text" class="form-control" id="judul_edit" name="judul_edit" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">File PDF :</label>
                            <input type="text" class="form-control" id="file_pdf_edit" name="file_pdf_edit" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Kategori :</label>
                            <select name="kategori_edit" id="kategori_edit" class="form-control" required>
                                <option value="">Pilih Kategori</option>
                                <option value="psikotest">Psikotes</option>
                                <option value="wawancara">Wawancara</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Nama Paket :</label>
                            <select name="paket_edit" id="paket_edit" class="selectpicker" data-live-search="true" data-width="100%" required>
                                <option value="">Pilih ...</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Status :</label>
                            <select name="status_id" id="status_id" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="0">Non Active</option>
                                <option value="1">Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

    </div>
    <!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->