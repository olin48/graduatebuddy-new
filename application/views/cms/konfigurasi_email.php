<!-- CONTENT -->
<div class="wrap-fluid" id="paper-bg">
    <div class="col-12">
        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <p style="font-size:15px;"><b>Konfigurasi Email</b> - Email Cara Bayar</p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_cara_bayar"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <p style="font-size:15px;"><b>Konfigurasi Email</b> - Email Konfirmasi</p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_konfirmasi"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <p style="font-size:15px;"><b>Konfigurasi Email</b> - Upgrade Paket</p>
                    <button class="btn-link d-edit" data-toggle="modal" data-target="#edit_upgrade_paket"><i class="fa fa-edit" style="color:blue; font-size:15px; padding-top:-50px;"></i></button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

    <div id="edit_cara_bayar" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfigurasi Email</h4>
                </div>

                <?php echo form_open_multipart('cms/update_email_cara_bayar'); ?>
                <div class="modal-body">
                    <?php foreach ($cara_bayar as $cb) : ?>
                        <div class="form-group">
                            <label for="usr">Subject :</label>
                            <input type="text" class="form-control" id="subject_cara_bayar" name="subject_cara_bayar" value="<?= $cb['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Message :</label>
                            <textarea class="form-control" id="message_cara_bayar" name="message_cara_bayar" style="height: 250px"><?= $cb['content']; ?></textarea>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_konfirmasi" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Konfigurasi Email</h4>
                </div>

                <?php echo form_open_multipart('cms/update_konfigurasi_email'); ?>
                <div class="modal-body">
                    <?php foreach ($config_email as $ce) : ?>
                        <div class="form-group">
                            <label for="usr">Subject :</label>
                            <input type="text" class="form-control" id="subject_email" name="subject_email" value="<?= $ce['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Message :</label>
                            <textarea class="form-control" id="message_email" name="message_email" style="height: 250px"><?= $ce['content']; ?></textarea>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="edit_upgrade_paket" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upgrade Paket</h4>
                </div>

                <?php echo form_open_multipart('cms/update_upgrade_paket'); ?>
                <div class="modal-body">
                    <?php foreach ($upgrade_paket as $up) : ?>
                        <div class="form-group">
                            <label for="usr">Subject :</label>
                            <input type="text" class="form-control" id="subject_upgrade_email" name="subject_upgrade_email" value="<?= $up['judul']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="usr">Message :</label>
                            <textarea class="form-control" id="message_upgrade_email" name="message_upgrade_email" style="height: 250px"><?= $up['content']; ?></textarea>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <!-- #/paper bg -->
</div>
<!-- ./wrap-sidebar-content -->

<!-- / END OF CONTENT -->