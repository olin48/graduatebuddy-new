<script type="text/javascript">
    (function($) {
        "use strict";
        CKEDITOR.replace('content_add');
        CKEDITOR.replace('lowongan_content_add');

        $('#tag_add').tagsInput();
        $('#lowongan_tag_add').tagsInput();

        $.getJSON(
            'kategori_blog_data',
            function(result) {
                $('#category_add').empty();
                $('#category_add').append('<option value="">Pilih ...</option>');
                $.each(result.result, function() {
                    $('#category_add').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                    $('#category_add').prop('disabled', false);
                });
                $('#category_add').selectpicker("refresh");
            }
        );

        $.getJSON(
            'kategori_lowongan_data',
            function(result) {
                $('#lowongan_category_add').empty();
                $('#lowongan_category_add').append('<option value="">Pilih ...</option>');
                $.each(result.result, function() {
                    $('#lowongan_category_add').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                    $('#lowongan_category_add').prop('disabled', false);
                });
                $('#lowongan_category_add').selectpicker("refresh");
            }
        );

        var dataBlog = $('#dataBlog').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_blog') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 6],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        var dataLowongan = $('#dataLowongan').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_lowongan') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 6],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function addKategoriBlog(form) {
        var kategori = prompt("Masukan kategori baru", "");
        if (kategori != null && kategori != "") {
            $.ajax({
                url: "<?php echo site_url('cms/add_kategori_blog/'); ?>" + kategori,
                type: "POST",
                data: {
                    category_name: kategori
                },
                cache: false,
                success: function(dataResult) {
                    $.getJSON(
                        'kategori_blog_data',
                        function(result) {
                            if (form == "add") {
                                $('#category_add').empty();
                                $('#category_add').append('<option value="">Pilih ...</option>');
                                $.each(result.result, function() {
                                    $('#category_add').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                                    $('#category_add').prop('disabled', false);
                                });
                                $('#category_add').selectpicker("refresh");
                            } else {
                                $('#category_edit').empty();
                                $('#category_edit').append('<option value="">Pilih ...</option>');
                                $.each(result.result, function() {
                                    $('#category_edit').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                                    $('#category_edit').prop('disabled', false);
                                });
                                $('#category_edit').selectpicker("refresh");
                            }
                        }
                    );
                }
            });
        } else {
            alert('Kategori harus diisi!');
        }
    }

    function addKategoriLowongan(form) {
        var kategori = prompt("Masukan kategori baru", "");
        if (kategori != null && kategori != "") {
            $.ajax({
                url: "<?php echo site_url('cms/add_kategori_lowongan/'); ?>" + kategori,
                type: "POST",
                data: {
                    category_name: kategori
                },
                cache: false,
                success: function(dataResult) {
                    $.getJSON(
                        'kategori_lowongan_data',
                        function(result) {
                            if (form == "add") {
                                $('#category_add').empty();
                                $('#category_add').append('<option value="">Pilih ...</option>');
                                $.each(result.result, function() {
                                    $('#category_add').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                                    $('#category_add').prop('disabled', false);
                                });
                                $('#category_add').selectpicker("refresh");
                            } else {
                                $('#category_edit').empty();
                                $('#category_edit').append('<option value="">Pilih ...</option>');
                                $.each(result.result, function() {
                                    $('#category_edit').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                                    $('#category_edit').prop('disabled', false);
                                });
                                $('#category_edit').selectpicker("refresh");
                            }
                        }
                    );
                }
            });
        } else {
            alert('Kategori harus diisi!');
        }
    }

    function edit_blog(id) {
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#image_edit').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_blog') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#blog_edit_id').val(data.id);
                $('#judul_edit').val(data.title);
                $('textarea#content_edit').val(data.post_text);
                $('#tag_edit').val(data.post_tag);
                $('#old_logo').val(data.image);

                $('#edit_blog').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
                $('#image_edit').append('<img src="<?php echo base_url('assets/uploads/single/'); ?>' + data.image + '" height="250px" style="margin-top: 10px;">');
                CKEDITOR.replace('content_edit');
                $('#tag_edit').tagsInput();

                $.getJSON(
                    'kategori_blog_data',
                    function(result) {
                        $('#category_edit').empty();
                        $('#category_edit').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            $('#category_edit').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                            $('select[name="category_edit"]').val(data.id_category);
                            $('.selectpicker').selectpicker('refresh');
                        });
                        $('#category_edit').selectpicker("refresh");
                    }
                );
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function edit_lowongan(id) {
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#lowongan_image_edit').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_lowongan') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#lowongan_edit_id').val(data.id);
                $('#lowongan_judul_edit').val(data.title);
                $('textarea#lowongan_content_edit').val(data.post_text);
                $('#lowongan_tag_edit').val(data.post_tag);
                $('#lowongan_old_logo').val(data.image);

                $('#edit_lowongan').modal('show'); // show bootstrap modal when complete loaded
                // $('.modal-title').text('Edit Menu'); // Set title to Bootstrap modal title
                $('#lowongan_image_edit').append('<img src="<?php echo base_url('assets/uploads/single/'); ?>' + data.image + '" height="150px" style="margin-top: 10px;">');
                CKEDITOR.replace('lowongan_content_edit');
                $('#lowongan_tag_edit').tagsInput();

                $.getJSON(
                    'kategori_lowongan_data',
                    function(result) {
                        $('#lowongan_category_edit').empty();
                        $('#lowongan_category_edit').append('<option value="">Pilih ...</option>');
                        $.each(result.result, function() {
                            $('#lowongan_category_edit').append('<option value="' + this['id'] + '">' + this['category_name'] + '</option>');
                            $('select[name="lowongan_category_edit"]').val(data.id_category);
                            $('.selectpicker').selectpicker('refresh');
                        });
                        $('#lowongan_category_edit').selectpicker("refresh");
                    }
                );
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_blog(id, image) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_blog/'); ?>" + id + '/' + image,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataBlog').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }

    function delete_lowongan(id, image) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_lowongan/'); ?>" + id + '/' + image,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataLowongan').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>