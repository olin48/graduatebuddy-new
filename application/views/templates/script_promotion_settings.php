<script type="text/javascript">
    (function($) {
        var dataTestimonial = $('#dataTestimonial').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_testimoni') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function edit_testimoni(id) {
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#image_edit').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_testimoni') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#testimoni_id_edit').val(data.id);
                $('#nama_member_edit').val(data.nama);
                $('#profesi_edit').val(data.profesi);
                $('#komentar_edit').val(data.komentar);
                $('#image_edit').append('<img src="<?php echo base_url('assets/uploads/single/'); ?>' + data.photo + '" height="250px" style="margin-top: 10px;">');
                $('#old_logo').val(data.photo);
                $('select[name="status_id"]').val(data.is_active);
                $('#edit_promosi').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }
</script>