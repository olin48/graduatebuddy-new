<script>
    (function($) {
        $.getJSON(
            'grafik_petani_data',
            function(result) {
                var petani = [];
                var data_harian = [];
                $.each(result.result, function() {
                    petani.push(this['first_name']);
                    data_harian.push(this['data_harian']);
                    var ctx = document.getElementById('grafik_harian').getContext('2d');
                    var chart = new Chart(ctx, {
                        // The type of chart we want to create
                        type: 'horizontalBar',

                        // The data for our dataset
                        data: {
                            labels: petani,
                            datasets: [{
                                label: 'Input Harian',
                                backgroundColor: 'rgb(140, 135, 251)',
                                data: data_harian
                            }]
                        },

                        // Configuration options go here
                        options: {
                            scales: {
                                xAxes: [{
                                    ticks: {
                                        min: 0,
                                        stepSize: 500
                                    }
                                }]
                            },
                            legend: {
                                position: 'bottom'
                            }
                        }
                    });
                });
            }
        );
    })(jQuery);
</script>

</Body>

</html>