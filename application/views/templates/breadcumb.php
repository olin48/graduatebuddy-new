<!-- Breadcrumb -->
<div class="sub-board">
    <span class="header-icon"><i class="fontello-home"></i>
    </span>
    <ol class="breadcrumb newcrumb ng-scope">
        <li>
            <a href="dashboard"><span></span>Dashboard</a>
        </li>
        <?php if ($menu_title != null) { ?>
            <?php echo "<li>"; ?>
            <?php echo $menu_title; ?>
            <?php echo "</li>"; ?>
        <?php } ?>
    </ol>
</div>
<!-- End of Breadcrumb -->