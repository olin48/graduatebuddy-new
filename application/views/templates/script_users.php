<script>
    (function($) {
        var dataUserAdmin = $('#dataUserAdmin').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_user_admin') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 6],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });

        var dataMember = $('#dataMember').dataTable({
            "processing": true,
            "serverSide": true,
            ajax: {
                "url": "<?= site_url('cms/get_data_member') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0, 4, 6, 7, 8],
                    "className": 'text-center'
                },
                {
                    "targets": [0],
                    "orderable": false
                }
            ]
        });
    })(jQuery);

    function profile_user_admin(id) {
        $('#form_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#photo_admin').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_user_admin') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#photo_admin').append("<img src='<?= base_url('assets/uploads/profile/'); ?>" + data.image + "'width='250px' height='250px'>");
                $('#show_profile_admin').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('View Photo Profile Admin'); // Set title to Bootstrap modal title
            }
        });
    }

    function view_user_admin(id) {
        $('#form_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#view_image').empty();
        $('#view_status').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_user_admin') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#view_id').text(data.id);
                $('#view_name').text(data.first_name + ' ' + data.last_name);
                $('#view_username').text(data.username);
                $('#view_email').text(data.email);
                $('#view_image').append("<img src='../assets/uploads/profile/" + data.image + "' width='80px' height='80px'>");
                $('#view_role').text(data.role);
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                } else if (data.is_active == 2) {
                    var status = "<td align='center'><span class='label label-danger'>Block</span></td>";
                }
                $('#view_status').append(status);
                $('#view_user_admin').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('View User Admin'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function edit_user_admin(id) {
        $('#form_edit_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#view_image').empty();
        $('#view_status').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_user_admin') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#edit_id').val(data.id);
                $('#edit_first_name').val(data.first_name);
                $('#edit_last_name').val(data.last_name);
                $('#edit_username').val(data.username);
                $('#edit_email').val(data.email);
                $('select[name="edit_role_id"]').val(data.role_id);
                $('select[name="edit_status_id"]').val(data.is_active);
                $('#view_image').append("<img src='../assets/uploads/profile/" + data.image + "' width='80px' height='80px'>");
                $('#view_role').text(data.role);
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                } else if (data.is_active == 2) {
                    var status = "<td align='center'><span class='label label-danger'>Block</span></td>";
                }
                $('#view_status').append(status);
                $('#edit_user_admin').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit User Admin'); // Set title to Bootstrap modal title
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_user_admin(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/delete_users_admin/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataUserAdmin').DataTable().ajax.reload();
                }
            });
        } else {
            return false;
        }
    }

    function view_member(id) {
        $('#form_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#view_photo_member').empty();
        $('#view_status_paket_member').empty();
        $('#view_status_member').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_member') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#view_id_member').text(data.id);
                $('#view_name_member').text(data.name);
                $('#view_email_member').text(data.email);
                $('#view_phone_member').text(data.phone);
                $('#view_photo_member').append("<img src='../assets/uploads/profile/" + data.photo + "' width='80px' height='80px'>");
                $('#view_umur_member').text(data.umur);
                $('#view_ttl_member').text(data.tempat_tgl_lahir);
                $('#view_universitas_member').text(data.universitas_name);
                $('#view_graduate_member').text(data.graduate_month_year);
                $('#view_paket_member').text(data.nama_paket);
                if (data.status_paket == 1) {
                    var status_peket = "<td align='center'><span class='label label-warning'>PENDING</span></td>";
                } else if (data.status_paket == 2) {
                    var status_peket = "<td align='center'><span class='label label-success'>ACTIVE</span></td>";
                } else {
                    var status_peket = "<td align='center'><span class='label label-danger'>INACTIVE</span></td>";
                }
                if (data.is_active == 0) {
                    var status = "<td align='center'><span class='label label-warning'>Not Active</span></td>";
                } else if (data.is_active == 1) {
                    var status = "<td align='center'><span class='label label-success'>Active</span></td>";
                } else if (data.is_active == 2) {
                    var status = "<td align='center'><span class='label label-danger'>Block</span></td>";
                }
                $('#view_status_paket_member').append(status_peket);
                $('#view_status_member').append(status);
                $('#view_member').modal('show'); // show bootstrap modal when complete loaded
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }

    function profile_member(id) {
        $('#form_user_admin').get(0).reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#photo_member').empty();
        $.ajax({
            url: "<?php echo site_url('cms/edit_data_member') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $('#photo_member').append("<img src='<?= base_url('assets/uploads/profile/'); ?>" + data.photo + "'width='250px' height='250px'>");
                $('#show_profile_member').modal('show'); // show bootstrap modal when complete loaded
            }
        });
    }

    function approve_member(id) {
        if (confirm('Apakah anda yakin ingin approve data member ini?')) {
            $.ajax({
                type: "POST",
                url: "<?= site_url('cms/approve_member/'); ?>" + id,
                data: {
                    id: id
                },
                success: function(data) {
                    $('#dataMember').DataTable().ajax.reload();
                    window.location.href = '<?= base_url('cms/users/member'); ?>';
                    $.ajax({
                        type: "POST",
                        url: "<?= site_url('cms/send_verification/'); ?>" + id,
                        data: {
                            id: id
                        },
                        success: function(data) {
                            alert('Approve data user ini berhasil!')
                        }
                    });
                }
            });
        } else {
            return false;
        }
    }
</script>

</body>

</html>