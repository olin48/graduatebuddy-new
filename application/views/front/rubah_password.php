<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <!-- <h2>Service</h2> 
						<p style="align: left"><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>Service</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--::review_part start::-->
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="section_tittle text-center">
                <p>Password baru anda</p>
                <h2>Rubah Password</h2>
            </div>
        </div>
        <form action="frontend/rubah_password" method="post" oninput='password2.setCustomValidity(password2.value != password1.value ? "Passwords do not match." : "")'>
            <div class="row justify-content-center">
                <div class="form-group" style="width: 50%;">
                    <label for="usr">Password Baru :</label>
                    <input type="password" class="form-control" id="password1" name="password1" required>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="form-group" style="width: 50%;">
                    <label for="usr">Ketik Ulang Password Baru :</label>
                    <input type="password" class="form-control" id="password2" name="password2" required>
                </div>
            </div>
            <div class="row justify-content-center">
                <button type="submit" class="btn hero-btn">Rubah Password</button>
            </div>
        </form>
    </div>
</section>