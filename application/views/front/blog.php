<!-- breadcrumb start-->
<section class="breadcrumb breadcrumb_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb_iner text-center">
                    <div class="breadcrumb_iner_item">
                        <!-- <h2>Our Blog</h2>
                        <p><a href="<?= base_url('home'); ?>" style="color: #f44a40">Home</a><span>/</span>Blog</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb start-->

<!--================Blog Area =================-->
<section class="blog_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-5 mb-lg-0">
                <div class="blog_left_sidebar">
                    <?php if (is_array($posts) && count($posts) > 0) {
                        foreach ($posts as $post) : ?>
                            <article class="blog_item">
                                <div class="blog_item_img">
                                    <img class="card-img rounded-0" src="<?= base_url('assets/uploads/single/'); ?><?= $post->image; ?>" alt="">
                                    <a href="#" class="blog_item_date">
                                        <h3><?= date('d', strtotime($post->created_date)); ?></h3>
                                        <p><?= date('M', strtotime($post->created_date)); ?></p>
                                    </a>
                                </div>

                                <div class="blog_details">
                                    <a class="d-inline-block" href="<?= base_url('blog/post/'); ?><?= $post->post_slug; ?>">
                                        <h2><?= $post->title; ?></h2>
                                    </a>
                                    <?= word_limiter($post->post_text, 35); ?>
                                    <ul class="blog-info-link">
                                        <li><a href="#"><i class="fa fa-tag"></i><?= $post->post_tag; ?></a></li>
                                        <li><a href="#"><i class="fa fa-comments"></i><?= $post->count_comment; ?> Komentar</a></li>
                                    </ul>
                                </div>
                            </article>
                        <?php endforeach;
                    } else { ?>
                        <article class="blog_item" style="text-align: center;">
                            <h3>Data tidak ditemukan!</h3>
                        </article>
                    <?php } ?>

                    <nav class="blog-pagination justify-content-center d-flex">
                        <?php echo $pagination; ?>
                    </nav>
                </div>
            </div>