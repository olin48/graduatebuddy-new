<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		    my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontend';
$route['maintenance'] = 'frontend/maintenance';
$route['home'] = 'frontend';
$route['about'] = 'frontend/about';
$route['reset_password'] = 'frontend/reset_password';
$route['rubah_password'] = 'frontend/rubah_password';
$route['free_cv_template'] = 'frontend/free_cv_template/';
// $route['blog'] = 'blog';
$route['service'] = 'frontend/service/';
$route['contact'] = 'frontend/contact';
$route['404_override'] = 'custom404';
$route['translate_uri_dashes'] = FALSE;

$route['member/home'] = 'member';
$route['member/paket'] = 'member/paket';
$route['member/profile'] = 'member/profile';

//view post
$route['blog'] = 'blog/page/';
$route['blog/page/(:any)'] = 'blog/page/';
$route['blog/post/(:any)'] = 'blog/view/$1';
$route['blog/tag/(:any)/(:any)'] = 'blog/tag/$1';
$route['blog/category/(:any)/(:any)'] = 'blog/category/$1';
$route['blog/search/(:any)'] = 'blog/search/$1';
$route['blog/search/(:any)'] = 'blog/search/$1';

$route['lowongan'] = 'lowongan/page/';
$route['lowongan/page/(:any)'] = 'lowongan/page/';
$route['lowongan/post/(:any)'] = 'lowongan/view/$1';
$route['lowongan/tag/(:any)/(:any)'] = 'lowongan/tag/$1';
$route['lowongan/category/(:any)/(:any)'] = 'lowongan/category/$1';
$route['lowongan/search/(:any)'] = 'lowongan/search/$1';
