<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Header_model extends CI_Model
{
    function update_header($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
}
