<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Footer_model extends CI_Model
{
    function config_data($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('cms_settings');
        return $query->result_array();
    }

    function update_footer($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
}
